import os

API_ENDPOINT = os.getenv('VODIA_PBX_API_ENDPOINT')
API_USER = os.getenv('VODIA_PBX_API_USER')
API_KEY = os.getenv('VODIA_PBX_API_KEY')