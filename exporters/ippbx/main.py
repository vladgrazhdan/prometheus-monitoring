#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : main.py
# desc    : IP PBX exporter to collect and export metrics to Prometheus 
# time    : 2023/09/16 17:28:53
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''


"""Vodia PBX exporter"""
import os
import time
from prometheus_client import start_http_server, Gauge
import requests
from vars import *

class VodiaMetrics:
    """
    Fetching and transforming Vodia PBX metrics into Prometheus metrics.
    """

    def __init__(self, pbx_port=443, polling_interval_seconds=10):
        self.pbx_port = pbx_port
        self.polling_interval_seconds = polling_interval_seconds

        # Prometheus metrics to collect
        self.cpu_max = Gauge("pbx_cpu_max", "CPU max")
        self.total_registrations = Gauge("pbx_registrations", "Registrations")
        self.total_memory = Gauge("pbx_memory", "RAM consumption")

    def run_metrics_loop(self):
        """Metrics fetching loop"""

        while True:
            self.fetch()
            time.sleep(self.polling_interval_seconds)

    def fetch(self):
        """
        Fetching metrics from the PBX and refreshing Prometheus with new values.
        """

        r_data = {}
        metrics = ['cpu-max', 'registrations', 'memory-all']
        for metric in metrics:
           r = requests.get(url = API_ENDPOINT, params={'type': metric}, auth = (API_USER,API_KEY))
           r_data[metric] = r.json()[-1]

        # Updating Prometheus metrics with application metrics
        self.cpu_max.set(r_data['cpu-max'])
        self.total_registrations.set(r_data['registrations'])
        self.total_memory.set(r_data['memory-all'])

def main():
    """Main entry point"""

    polling_interval_seconds = int(os.getenv("POLLING_INTERVAL_SECONDS", "10"))
    pbx_port = int(os.getenv("PBX_PORT", "443"))
    exporter_port = int(os.getenv("EXPORTER_PORT", "9878"))

    pbx_metrics = VodiaMetrics(
        pbx_port=pbx_port,
        polling_interval_seconds=polling_interval_seconds
    )
    start_http_server(exporter_port)
    pbx_metrics.run_metrics_loop()

if __name__ == "__main__":
    main()