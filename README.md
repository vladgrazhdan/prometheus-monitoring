# Prometheus monitoring

## Intro

Kubernetes manifests for a DB-driven Java app and monitoring with Prometheus.

A custom-built Prometheus exporter to collect and export metrics to Prometheus.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Prometheus 2.33.4+
* Python 3.10+

### Install

Use git to clone this repository locally.
